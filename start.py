import time

from flask import Flask, Response, request, jsonify
from flask_cors import CORS
import requests
import json


from deal_generate.RandomDealData import RandomDealData

def create_app():
    '''Create an app by initializing components'''
    application = Flask(__name__)
    return application

application = create_app()
CORS(application)

@application.route('/mainStream')
def stream():
    def eventStream():
        while True:
            data = get_message()
            dao_config = datagen_server_config('dao-server')
            dao_url = dao_config['url']
            r_dao = requests.post(dao_url, json=json.loads(data))
            yield data+"\n"
            try:
                if r_dao.json()['status'] == 200:
                    return jsonify(status=200, msg='OK')
                else:
                    return jsonify(status=403, msg='Not ok')
            except Exception:
                pass
    return Response(eventStream(), mimetype="text/event-stream")


def get_message():
    randomDealDate = RandomDealData()
    listInstruments = randomDealDate.createInstrumentList()
    s = randomDealDate.createRandomData(listInstruments)
    return s


def datagen_server_config(server):
    with open('./config/server-config.json') as f:
        config = json.load(f)
    return config[server]

if __name__ == '__main__':
    config = datagen_server_config('data-server')
    print("_______________Running in debug mode_______________")
    application.run(port=config['port'], threaded=True, host=(config['host']))


